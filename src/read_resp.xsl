<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope"
  xmlns:SOAP-ENC="http://www.w3.org/2003/05/soap-encoding"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  xmlns:ns4="http://opcfoundation.org/UA/2008/02/Endpoints.wsdl/BasicHttpBinding_ISessionEndpoint"
  xmlns:ns5="http://opcfoundation.org/UA/2008/02/Endpoints.wsdl/BasicHttpBinding_IDiscoveryEndpoint"
  xmlns:ns6="http://opcfoundation.org/UA/2008/02/Endpoints.wsdl/BasicHttpBinding_IRegistrationEndpoint"
  xmlns:ns3="http://opcfoundation.org/UA/2008/02/Types.xsd">
	<xsl:template match="/">
	<data>
	[
		<xsl:for-each select="SOAP-ENV:Envelope/SOAP-ENV:Body/ns3:ReadResponse/ns3:Results/ns3:DataValue">
			{
			"fake" : "fake",
			<xsl:for-each select="ns3:Value">
				"value" : 
					<xsl:for-each select="ns3:Boolean">
						"<xsl:value-of select="."/>",
					</xsl:for-each>
					<xsl:for-each select="ns3:Byte">
						"<xsl:value-of select="."/>",
					</xsl:for-each>
					<xsl:for-each select="ns3:SByte">
						"<xsl:value-of select="."/>",
					</xsl:for-each>
					<xsl:for-each select="ns3:Int16">
						"<xsl:value-of select="."/>",
					</xsl:for-each>
					<xsl:for-each select="ns3:UInt16">
						"<xsl:value-of select="."/>",
					</xsl:for-each>
					<xsl:for-each select="ns3:Int32">
						"<xsl:value-of select="."/>",
					</xsl:for-each>
					<xsl:for-each select="ns3:UInt32">
						"<xsl:value-of select="."/>",
					</xsl:for-each>
					<xsl:for-each select="ns3:Int64">
						"<xsl:value-of select="."/>",
					</xsl:for-each>
					<xsl:for-each select="ns3:UInt64">
						"<xsl:value-of select="."/>",
					</xsl:for-each>
					<xsl:for-each select="ns3:Float">
						"<xsl:value-of select="."/>",
					</xsl:for-each>
					<xsl:for-each select="ns3:Double">
						"<xsl:value-of select="."/>",
					</xsl:for-each>
					<xsl:for-each select="ns3:String">
						"<xsl:value-of select="."/>",
					</xsl:for-each>
					<xsl:for-each select="ns3:DateTime">
						"<xsl:value-of select="."/>",
					</xsl:for-each>
					<xsl:for-each select="ns3:Guid">
						"<xsl:value-of select="ns3:String"/>",
					</xsl:for-each>
					<xsl:for-each select="ns3:ByteString">
						"<xsl:value-of select="."/>",
					</xsl:for-each>
					<xsl:for-each select="ns3:NodeId">
						"<xsl:value-of select="ns3:Identifier"/>",
					</xsl:for-each>
					<xsl:for-each select="ns3:ExpandedNodeId">
						"<xsl:value-of select="ns3:Identifier"/>",
					</xsl:for-each>
					<xsl:for-each select="ns3:StatusCode">
						"<xsl:value-of select="ns3:Code"/>",
					</xsl:for-each>
					<xsl:for-each select="ns3:QualifiedName">
						"<xsl:value-of select="ns3:NamespaceIndex"/>,<xsl:value-of select="ns3:Name"/>", 
					</xsl:for-each>
					<xsl:for-each select="ns3:LocalizedText">
						"<xsl:value-of select="ns3:Text"/>",
					</xsl:for-each>
 			</xsl:for-each>
			<xsl:for-each select="ns3:StatusCode">
				"status" : "<xsl:value-of select="ns3:Code"/>",
			</xsl:for-each>
			"fake1" : "fake1"
			}
			<xsl:if test="position() != last()">,</xsl:if>
		</xsl:for-each>
		]
	</data>
	</xsl:template>
</xsl:stylesheet> 
