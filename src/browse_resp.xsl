<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope"
  xmlns:SOAP-ENC="http://www.w3.org/2003/05/soap-encoding"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  xmlns:ns4="http://opcfoundation.org/UA/2008/02/Endpoints.wsdl/BasicHttpBinding_ISessionEndpoint"
  xmlns:ns5="http://opcfoundation.org/UA/2008/02/Endpoints.wsdl/BasicHttpBinding_IDiscoveryEndpoint"
  xmlns:ns6="http://opcfoundation.org/UA/2008/02/Endpoints.wsdl/BasicHttpBinding_IRegistrationEndpoint"
  xmlns:ns3="http://opcfoundation.org/UA/2008/02/Types.xsd">
	<xsl:template match="/">
	<data>
		<div class="node_children">
		<xsl:for-each select="SOAP-ENV:Envelope/SOAP-ENV:Body/ns3:BrowseResponse/ns3:Results/ns3:BrowseResult">
			<xsl:for-each select="ns3:References/ns3:ReferenceDescription">
			<div class="node">
				<xsl:attribute name="id"><xsl:value-of select="ns3:NodeId/ns3:Identifier"/></xsl:attribute>            
				<div class="node_content">
					<div class="node_state" onclick="toggleState(event)">+</div>
					<div class="node_label" onclick="showAttributes(event)"><xsl:value-of select="ns3:BrowseName/ns3:Name"/></div>
				</div>
			</div>
			</xsl:for-each>
		</xsl:for-each>
		</div>
	</data>
	</xsl:template>
</xsl:stylesheet> 
