function opcuaSite()
{
    this._get = function(documentUrl, onDataReceived)
    {
      $.ajax(documentUrl, {
        async : false,
        context : this,
        dataType: "xml"
      })
      .done(onDataReceived)
      .fail(function(jqXHR, textStatus, errorThrown){
        alert("Filedto load " + documentUrl + ": " + textStatus);
      });
    };
    
    this._get("get_endpoints_resp.xsl", function(xslt)
    {
      var xsltProcessor=new XSLTProcessor();
      xsltProcessor.importStylesheet(xslt);
      this.getEndpointsXslt = xsltProcessor;
    });

    this._get("browse_resp.xsl", function(xslt)
    {
      var xsltProcessor=new XSLTProcessor();
      xsltProcessor.importStylesheet(xslt);
      this.browseXslt = xsltProcessor;
    });

    this._get("read_resp.xsl", function(xslt)
    {
      var xsltProcessor=new XSLTProcessor();
      xsltProcessor.importStylesheet(xslt);
      this.readXslt = xsltProcessor;
    });
};

var client = new opcuaSite();

function setEndpoints(endpoints)
{
  var resultDocument = client.getEndpointsXslt.transformToFragment(endpoints, document);
  if (resultDocument)
  {
    var elem = document.getElementById("endpoints"); 
    elem.innerHTML = resultDocument.firstChild.innerHTML; 
  }
};

function onBrowse(id, browsed, node)
{
  var resultDocument = client.browseXslt.transformToFragment(browsed, document);
  if (resultDocument)
  {
    $(node).append($(resultDocument.firstChild).find(".node_children")).on("click", toggleState);
    $(node).find(".node_state:first").html("-");
  }
}

function onRead(ids, attributes, node) 
{
  var resultDocument = client.readXslt.transformToFragment(attributes, document);
  var json = jQuery.parseJSON(resultDocument.firstChild.innerHTML);
  if (json.length != ids.length)
  {
    throw "invalid number of attributes received.";
  }
  for (var i = 0; i < ids.length; i++)
  {
    var attrId = "#attr" + ids[i].attribute; 
    if (json[i].value)
    {
      $(attrId).html(json[i].value);
    }
    else
    {
      $(attrId).html("");
    }
  }
}

function toggleState(event)
{
  event.stopPropagation();
  if (event.currentTarget.getAttribute("class") != "node_state")
    return;

  var node = event.currentTarget.parentNode.parentNode;
  var length = $(node).has(".node_children").length; 
  if (length)
  { 
    $(node.childNodes).remove(".node_children");
    $(node).find(".node_state:first").html("+");
  }
  else
  {
    var id = node.getAttribute("id");
    opcua.browse(id, onBrowse, node);
  }
}

function showAttributes(event)
{
  event.stopPropagation();
  if (event.currentTarget.getAttribute("class") != "node_label")
    return;

  var node = event.currentTarget.parentNode.parentNode;
  var id = node.getAttribute("id");
  opcuaSettings.nodeID = id;
  var ids = 
    [
      { nodeId : id, attribute : opcua.attributeIds.nodeId},
      { nodeId : id, attribute : opcua.attributeIds.nodeClass},
      { nodeId : id, attribute : opcua.attributeIds.browseName},
      { nodeId : id, attribute : opcua.attributeIds.displayName},
      { nodeId : id, attribute : opcua.attributeIds.description},
      { nodeId : id, attribute : opcua.attributeIds.writeMask},
      { nodeId : id, attribute : opcua.attributeIds.userWriteMask},
      { nodeId : id, attribute : opcua.attributeIds.isAbstract},
      { nodeId : id, attribute : opcua.attributeIds.symmetric},
      { nodeId : id, attribute : opcua.attributeIds.inverseName},
      { nodeId : id, attribute : opcua.attributeIds.containsNoLoops},
      { nodeId : id, attribute : opcua.attributeIds.eventNotifier},
      { nodeId : id, attribute : opcua.attributeIds.value},
      { nodeId : id, attribute : opcua.attributeIds.dataType},
      { nodeId : id, attribute : opcua.attributeIds.valueRank},
      { nodeId : id, attribute : opcua.attributeIds.arrayDimensions},
      { nodeId : id, attribute : opcua.attributeIds.accessLevel},
      { nodeId : id, attribute : opcua.attributeIds.userAccessLevel},
      { nodeId : id, attribute : opcua.attributeIds.minimumSamplingInterval},
      { nodeId : id, attribute : opcua.attributeIds.historizing},
      { nodeId : id, attribute : opcua.attributeIds.executable},
      { nodeId : id, attribute : opcua.attributeIds.userExecutable},
    ];

  opcua.read(ids, onRead, node);
}

function getAttributeID(name)
{
  switch (name)
  {
    case "attr1": return opcua.attributeIds.nodeId;                   
    case "attr2": return opcua.attributeIds.nodeClass;                
    case "attr3": return opcua.attributeIds.browseName;
    case "attr4": return opcua.attributeIds.displayName;              
    case "attr5": return opcua.attributeIds.description;              
    case "attr6": return opcua.attributeIds.writeMask;                
    case "attr7": return opcua.attributeIds.userWriteMask;            
    case "attr8": return opcua.attributeIds.isAbstract;               
    case "attr9": return opcua.attributeIds.symmetric;                
    case "attr10": return opcua.attributeIds.inverseName;              
    case "attr11": return opcua.attributeIds.containsNoLoops;          
    case "attr12": return opcua.attributeIds.eventNotifier;            
    case "attr13": return opcua.attributeIds.value;                    
    case "attr14": return opcua.attributeIds.dataType;                 
    case "attr15": return opcua.attributeIds.valueRank;                
    case "attr16": return opcua.attributeIds.arrayDimensions;          
    case "attr17": return opcua.attributeIds.accessLevel;              
    case "attr18": return opcua.attributeIds.userAccessLevel;          
    case "attr19": return opcua.attributeIds.minimumSamplingInterval;  
    case "attr20": return opcua.attributeIds.historizing;              
    case "attr21": return opcua.attributeIds.executable;            
    case "attr22": return opcua.attributeIds.userExecutable;
  }
  return opcua.attributeIds.value;
}

function writeValue(nodeId, attributeName, value)
{
  var writeValue = {nodeId : nodeId, attribute: getAttributeID(attributeName), value : value};
  opcua.write(writeValue);
}

$(document).ready(function(){

  $("td[id^=attr]").editInPlace({
    callback: function(original_element, html, original){
      if (opcuaSettings.nodeID)
        writeValue(opcuaSettings.nodeID, original_element, html);

      return html;
    }
  });

  opcua.getEndpoints(setEndpoints);
});
