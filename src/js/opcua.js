var opcua = {
  soapHeader : "<?xml version=\"1.0\" encoding=\"UTF-8\"?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:SOAP-ENC=\"http://www.w3.org/2003/05/soap-encoding\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:ns4=\"http://opcfoundation.org/UA/2008/02/Endpoints.wsdl/BasicHttpBinding_ISessionEndpoint\" xmlns:ns5=\"http://opcfoundation.org/UA/2008/02/Endpoints.wsdl/BasicHttpBinding_IDiscoveryEndpoint\" xmlns:ns6=\"http://opcfoundation.org/UA/2008/02/Endpoints.wsdl/BasicHttpBinding_IRegistrationEndpoint\" xmlns:ns3=\"http://opcfoundation.org/UA/2008/02/Types.xsd\"> <SOAP-ENV:Body>",
  soapFooter : "</SOAP-ENV:Body></SOAP-ENV:Envelope>",    
  requestHeader : "<ns3:RequestHeader><ns3:AuthenticationToken><ns3:Identifier></ns3:Identifier></ns3:AuthenticationToken><ns3:Timestamp>2013-10-19T21:57:30Z</ns3:Timestamp><ns3:RequestHandle>0</ns3:RequestHandle>     <ns3:ReturnDiagnostics>0</ns3:ReturnDiagnostics>     <ns3:AuditEntryId></ns3:AuditEntryId>     <ns3:TimeoutHint>0</ns3:TimeoutHint>     <ns3:AdditionalHeader>      <ns3:TypeId>       <ns3:Identifier></ns3:Identifier>       <!-- extensibility element(s) -->      </ns3:TypeId> <ns3:Body></ns3:Body></ns3:AdditionalHeader></ns3:RequestHeader>",   

  getEndpoints : function(onEndpoints) {
    var request = "<ns3:GetEndpointsRequest>";
    request += this.requestHeader;
    request += "<ns3:EndpointUrl></ns3:EndpointUrl>";
    request += "<ns3:LocaleIds></ns3:LocaleIds>";
    request += "<ns3:ProfileUris></ns3:ProfileUris>";
    request += "</ns3:GetEndpointsRequest>";
    
    this._process(opcuaSettings.discoveryServer, request, onEndpoints);
  },

  browse : function(nodeId, onBrowse, param)
  {
    var request = "<ns3:BrowseRequest>";
    request += this.requestHeader;
    request += "<ns3:View><ns3:ViewId><ns3:Identifier></ns3:Identifier></ns3:ViewId><ns3:Timestamp>2013-10-19T21:57:30Z</ns3:Timestamp><ns3:ViewVersion>0</ns3:ViewVersion></ns3:View>";
    request += "<ns3:RequestedMaxReferencesPerNode>0</ns3:RequestedMaxReferencesPerNode>";
    request += "<ns3:NodesToBrowse>";
    request += "<ns3:BrowseDescription>";
    request += "<ns3:NodeId><ns3:Identifier>" + nodeId + "</ns3:Identifier></ns3:NodeId>";
    request += "<ns3:BrowseDirection>Forward_0</ns3:BrowseDirection>";
    request += "<ns3:ReferenceTypeId>";
    request += "<ns3:Identifier></ns3:Identifier>";
    request += "</ns3:ReferenceTypeId>";
    request += "<ns3:IncludeSubtypes>false</ns3:IncludeSubtypes>";
    request += "<ns3:NodeClassMask>0</ns3:NodeClassMask>";
    request += "<ns3:ResultMask>0</ns3:ResultMask>";
    request += "</ns3:BrowseDescription>";
    request += "</ns3:NodesToBrowse>";
    request += "</ns3:BrowseRequest>";
    
    this._process(opcuaSettings.endpointURL, request, function(resp)
    {
      onBrowse(nodeId, resp, param);
    });
  },
  
  
  read : function(ids, onRead, param)
  {
    var request = "<ns3:ReadRequest>";
    request += this.requestHeader;
    request += "<ns3:NodesToRead>";
    for (var i = 0; i < ids.length; i++)
    {  
      request += "<ns3:ReadValueId>";
      request += "<ns3:NodeId><ns3:Identifier>" + ids[i].nodeId + "</ns3:Identifier></ns3:NodeId>";
      request += "<ns3:AttributeId>" + ids[i].attribute + "</ns3:AttributeId>";
      //<ns3:IndexRange></ns3:IndexRange>
      //<ns3:DataEncoding>
      //<ns3:NamespaceIndex>0</ns3:NamespaceIndex>
      //<ns3:Name></ns3:Name>
      //</ns3:DataEncoding>
      request += "</ns3:ReadValueId>";
    }
    request += "</ns3:NodesToRead>";
    request += "</ns3:ReadRequest>";
    
    this._process(opcuaSettings.endpointURL, request, function(resp)
    {
      if (onRead)
      {
        onRead(ids, resp, param);
      }
    });
  },

  write : function(value, onWrite, param)
  {
    var request = "<ns3:WriteRequest>"
    request += this.requestHeader;
    request += "<ns3:NodesToWrite>";
    request += "<ns3:WriteValue>";
    request += "<ns3:NodeId><ns3:Identifier>" + value.nodeId + "</ns3:Identifier></ns3:NodeId>";
    request += "<ns3:AttributeId>" + value.attribute + "</ns3:AttributeId>";
    //<ns3:IndexRange></ns3:IndexRange>
    request += "<ns3:Value>";
    request += "<ns3:Value><ns3:String>" + value.value + "</ns3:String></ns3:Value>";
    //request += "<ns3:StatusCode><ns3:Code></ns3:Code></ns3:StatusCode>";
    //request += "<ns3:SourceTimestamp>2013-12-22T12:24:40Z</ns3:SourceTimestamp>";
    //request += "<ns3:ServerTimestamp>2013-12-22T12:24:40Z</ns3:ServerTimestamp>";
    request += "</ns3:Value>";
    
    request += "</ns3:WriteValue>";
    request += "</ns3:NodesToWrite>";
    request += "</ns3:WriteRequest>"
    
    this._process(opcuaSettings.endpointURL, request, function(resp){
      if (onWrite)
      {
        onWrite(param);
      }
    });
  },
  
  onError : function(jqXHR, textStatus, errorThrown)
  {
    alert("Error: " + errorThrown.toString());
  },
  
  _process : function (url, request, onDataReceived)
  {
    var soapRequest = this.soapHeader + request + this.soapFooter;
    $.ajax({
      async : false,
      type: "POST",
      url: url,
      dataType: "xml",
      data: soapRequest, 
    })
    .done(onDataReceived)
    .fail(this.onError);
  },

  /**
   * Attribute ids
   */
  attributeIds : {
      nodeId : 1,
      nodeClass : 2,
      browseName : 3,
      displayName : 4,
      description : 5,
      writeMask : 6,
      userWriteMask : 7,
      isAbstract : 8,
      symmetric : 9,
      inverseName : 10,
      containsNoLoops : 11,
      eventNotifier : 12,
      value : 13,
      dataType : 14,
      valueRank : 15,
      arrayDimensions : 16,
      accessLevel : 17,
      userAccessLevel : 18,
      minimumSamplingInterval : 19,
      historizing : 20,
      executable : 21,
      userExecutable : 22
  }
};
