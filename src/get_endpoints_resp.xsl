<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope"
  xmlns:SOAP-ENC="http://www.w3.org/2003/05/soap-encoding"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  xmlns:ns4="http://opcfoundation.org/UA/2008/02/Endpoints.wsdl/BasicHttpBinding_ISessionEndpoint"
  xmlns:ns5="http://opcfoundation.org/UA/2008/02/Endpoints.wsdl/BasicHttpBinding_IDiscoveryEndpoint"
  xmlns:ns6="http://opcfoundation.org/UA/2008/02/Endpoints.wsdl/BasicHttpBinding_IRegistrationEndpoint"
  xmlns:ns3="http://opcfoundation.org/UA/2008/02/Types.xsd">
	<xsl:template match="/">
	<data>
        <xsl:for-each select="SOAP-ENV:Envelope/SOAP-ENV:Body/ns3:GetEndpointsResponse/ns3:Endpoints/ns3:EndpointDescription">
	        <xsl:choose>
	          <xsl:when test="position() = 1">
	           <li class="active">
	             <a href="#">
	               <xsl:attribute name="onclick">alert("<xsl:value-of select="ns3:EndpointUrl"/>")</xsl:attribute>
	               <xsl:value-of select="ns3:EndpointUrl"/>
	             </a>
	           </li>
	          </xsl:when>
	          <xsl:otherwise>
             <li>
               <a href="#">
                 <xsl:attribute name="onclick">alert("<xsl:value-of select="ns3:EndpointUrl"/>")</xsl:attribute>
                 <xsl:value-of select="ns3:EndpointUrl"/>
               </a>
             </li>
	          </xsl:otherwise>
	        </xsl:choose>
        </xsl:for-each>
  </data>
	</xsl:template>
</xsl:stylesheet> 